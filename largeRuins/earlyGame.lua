
return function(center) --early game setups
    local ce = game.surfaces[1].create_entity --save typing
    local fRN = game.forces['ruins_neutral']
    local direct = defines.direction

    ce{name = "lab", position = {center.x + (-7.0), center.y + (-10.0)}, force = fRN}
    ce{name = "small-electric-pole", position = {center.x + (-5.0), center.y + (-10.0)}, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (-5.0), center.y + (-11.0)}, direction = direct.west, force = fRN}
    ce{name = "lab", position = {center.x + (-3.0), center.y + (-10.0)}, force = fRN}
    ce{name = "lab", position = {center.x + (1.0), center.y + (-10.0)}, force = fRN}
    ce{name = "small-electric-pole", position = {center.x + (3.0), center.y + (-10.0)}, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (3.0), center.y + (-11.0)}, direction = direct.west, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (-1.0), center.y + (-9.0)}, direction = direct.west, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (4.0), center.y + (-8.0)}, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (3.0), center.y + (-9.0)}, direction = direct.west, force = fRN}
    ce{name = "small-electric-pole", position = {center.x + (5.0), center.y + (-8.0)}, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (6.0), center.y + (-8.0)}, force = fRN}
    ce{name = "lab", position = {center.x + (-7.0), center.y + (-6.0)}, force = fRN}
    ce{name = "small-electric-pole", position = {center.x + (-5.0), center.y + (-6.0)}, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (-5.0), center.y + (-7.0)}, direction = direct.east, force = fRN}
    ce{name = "lab", position = {center.x + (-3.0), center.y + (-6.0)}, force = fRN}
    ce{name = "small-electric-pole", position = {center.x + (-1.0), center.y + (-6.0)}, force = fRN}
    ce{name = "lab", position = {center.x + (1.0), center.y + (-6.0)}, force = fRN}
    ce{name = "lab", position = {center.x + (5.0), center.y + (-6.0)}, force = fRN}
    ce{name = "small-electric-pole", position = {center.x + (3.0), center.y + (-6.0)}, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (3.0), center.y + (-7.0)}, direction = direct.east, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (3.0), center.y + (-5.0)}, direction = direct.east, force = fRN}
    ce{name = "radar", position = {center.x + (7.0), center.y + (0.0)}, force = fRN}
    ce{name = "transport-belt", position = {center.x + (-7.0), center.y + (2.0)}, direction = direct.east, force = fRN}
    ce{name = "transport-belt", position = {center.x + (-4.0), center.y + (2.0)}, direction = direct.south, force = fRN}
    ce{name = "transport-belt", position = {center.x + (-5.0), center.y + (2.0)}, direction = direct.east, force = fRN}
    ce{name = "stone-furnace", position = {center.x + (-1.5), center.y + (1.5)}, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (0.0), center.y + (2.0)}, direction = direct.west, force = fRN}
    ce{name = "transport-belt", position = {center.x + (-4.0), center.y + (4.0)}, direction = direct.south, force = fRN}
    ce{name = "transport-belt", position = {center.x + (-4.0), center.y + (3.0)}, direction = direct.south, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (-3.0), center.y + (4.0)}, direction = direct.west, force = fRN}
    ce{name = "stone-furnace", position = {center.x + (-1.5), center.y + (3.5)}, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (0.0), center.y + (4.0)}, direction = direct.west, force = fRN}
    ce{name = "transport-belt", position = {center.x + (1.0), center.y + (4.0)}, direction = direct.south, force = fRN}
    ce{name = "transport-belt", position = {center.x + (1.0), center.y + (3.0)}, direction = direct.south, force = fRN}
    ce{name = "transport-belt", position = {center.x + (-4.0), center.y + (6.0)}, direction = direct.south, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (-3.0), center.y + (6.0)}, direction = direct.west, force = fRN}
    ce{name = "stone-furnace", position = {center.x + (-1.5), center.y + (5.5)}, force = fRN}
    ce{name = "transport-belt", position = {center.x + (1.0), center.y + (5.0)}, direction = direct.south, force = fRN}
    ce{name = "transport-belt", position = {center.x + (-4.0), center.y + (8.0)}, direction = direct.south, force = fRN}
    ce{name = "transport-belt", position = {center.x + (-4.0), center.y + (7.0)}, direction = direct.south, force = fRN}
    ce{name = "stone-furnace", position = {center.x + (-1.5), center.y + (7.5)}, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (0.0), center.y + (8.0)}, direction = direct.west, force = fRN}
    ce{name = "transport-belt", position = {center.x + (1.0), center.y + (8.0)}, direction = direct.south, force = fRN}
    ce{name = "transport-belt", position = {center.x + (1.0), center.y + (7.0)}, direction = direct.south, force = fRN}
    ce{name = "transport-belt", position = {center.x + (-4.0), center.y + (10.0)}, direction = direct.south, force = fRN}
    ce{name = "fast-inserter", position = {center.x + (-3.0), center.y + (10.0)}, direction = direct.west, force = fRN}
    ce{name = "stone-furnace", position = {center.x + (-1.5), center.y + (9.5)}, force = fRN}
    ce{name = "transport-belt", position = {center.x + (1.0), center.y + (9.0)}, direction = direct.south, force = fRN}

end
