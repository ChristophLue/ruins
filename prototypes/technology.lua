data:extend({
  {
    type = "technology",
    name = "ruins-assimilation",
    prerequisites =
    {
      "personal-roboport-equipment",
    },
    icon = "__Ruins__/graphics/technology/ruins-assimilator.png",
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "ruins-assimilator"
      },
    },
    unit =
    {
      count = 100,
      ingredients =
      {
        {"science-pack-1", 1},
        {"science-pack-2", 1},
        {"science-pack-3", 1},
      },
      time = 30
    },
    icon_size = 128,
  }
})
