data:extend({
  {
    type = "selection-tool",
    name = "ruins-assimilator",
    icon = "__Ruins__/graphics/icons/ruins-assimilator.png",
    stack_size = 1,
    subgroup = "tool",
    order = "c[automated-construction]-d[ruins-assimilator]",
    flags = {"goes-to-quickbar"},
    selection_color = {r = 1.0, g = 0.2, b = 1.0, a = 0.3},
    alt_selection_color = {r = 0.2, g = 0.8, b = 0.3, a = 0.3},
    selection_mode = {"buildable-type"},
    alt_selection_mode = {"buildable-type"},
    selection_cursor_box_type = "entity",
    alt_selection_cursor_box_type = "entity"
  },
  {
    type = "recipe",
    name = "ruins-assimilator",
    enabled = false,
    energy_required = 1,
    ingredients =
    {
      {"advanced-circuit", 1},
    },
    result = "ruins-assimilator"
  }
})
