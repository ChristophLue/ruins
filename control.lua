require("smallRuins")
require("mediumRuins")
require("largeRuins")

local DEBUG = false --used for debug, users should not enable

--function that will return true 'percent' of the time.
function probability(percent)
    return math.random() <= percent
end

script.on_event({defines.events.on_chunk_generated},
    function (e)
        local center = {x=(e.area.left_top.x+e.area.right_bottom.x)/2, y=(e.area.left_top.y+e.area.right_bottom.y)/2}
        if math.abs(center.x) < settings.global["ruins-min-distance-from-spawn"].value and math.abs(center.y) < settings.global["ruins-min-distance-from-spawn"].value then return end --too close to spawn

        if probability(settings.global["ruins-small-ruin-chance"].value) then
            --spawn small ruin
            if DEBUG then
                game.print("A small ruin was spawned at " .. center.x .. "," .. center.y)
            end

            --random variance so they aren't always chunk aligned
            center.x = center.x + math.random(-10,10)
            center.y = center.y + math.random(-10,10)

            spawnSmallRuins(center)
        elseif probability(settings.global["ruins-medium-ruin-chance"].value) then
            --spawn medium ruin
            if DEBUG then
                game.print("A medium ruin was spawned at " .. center.x .. "," .. center.y)
            end

            --random variance so they aren't always chunk aligned
            center.x = center.x + math.random(-5,5)
            center.y = center.y + math.random(-5,5)

            spawnMediumRuins(center)
        elseif probability(settings.global["ruins-large-ruin-chance"].value) then
            --spawn large ruin
            if DEBUG then
                game.print("A large ruin was spawned at " .. center.x .. "," .. center.y)
            end
            spawnLargeRuins(center)
        end
    end
)

script.on_init(
    function()
        math.randomseed(game.surfaces[1].map_gen_settings.seed) --set the random seed to the map seed, so ruins are the same-ish with each generation.
    end
)


local function InitRuinsFactions()

  if game.forces['ruins_neutral'] == nil then
    game.create_force('ruins_neutral')
  end
  if game.forces['ruins_enemy'] == nil then
    game.create_force('ruins_enemy')
  end

  -- Note: no idea if all of this settings are necessary,...
  -- todo: likey sufficient to do this only when setting up the forces above
  -- todo: mines in 'ruins_enemy' group still explode on creation
  game.forces['ruins_neutral'].set_cease_fire('enemy', true)
  game.forces['ruins_neutral'].set_cease_fire('neutral', true)
  game.forces['ruins_neutral'].set_cease_fire('player', true)
  game.forces['ruins_neutral'].set_friend('player', true)
  game.forces['ruins_neutral'].set_cease_fire('ruins_enemy', true)
  game.forces['ruins_neutral'].set_friend('ruins_enemy', true)
  game.forces['ruins_enemy'].set_cease_fire('enemy', true)
  game.forces['ruins_enemy'].set_friend('enemy', true)
  game.forces['ruins_enemy'].set_cease_fire('neutral', true)
  game.forces['ruins_enemy'].set_friend('neutral', true)
  game.forces['ruins_enemy'].set_cease_fire('ruins_neutral', true)
  game.forces['ruins_enemy'].set_friend('ruins_neutral', true)
  game.forces['player'].set_cease_fire('ruins_neutral', true)
  game.forces['player'].set_friend('ruins_neutral', true)
  game.forces['enemy'].set_cease_fire('ruins_neutral', true)
  game.forces['enemy'].set_cease_fire('ruins_enemy', true)
  game.forces['enemy'].set_friend('ruins_enemy', true)
end

local function assimilateRuins(event, deconstruct)
  local playerfaction = game.players[event.player_index].force
  local numAssimilated = 0
  local numNeutralzied = 0

  for _,entity in pairs(event.entities) do
    -- convert ruins_neutral --> player
    -- order deconstruction of new player force buildings
    if entity.force.name == 'ruins_neutral' then
      entity.force = playerfaction
      numAssimilated = numAssimilated + 1
      if deconstruct == True then
        entity.order_deconstruction(playerfaction)
      end
    end
    -- convert ruins_enemy --> game.forces.neutral
    if entity.force.name == 'ruins_enemy' then
      entity.force = game.forces.neutral
      numNeutralzied = numNeutralzied + 1
    end
  end
  if numAssimilated > 0 then
    if numNeutralzied > 0 then
      game.players[event.player_index].print(numAssimilated .. " entities assimilated and " .. numNeutralzied .. " entities neutralized.")
    else
      game.players[event.player_index].print(numAssimilated .. " entities assimilated.")
    end
  elseif numNeutralzied > 0 then
    game.players[event.player_index].print(numNeutralzied .. " entities neutralized.")
  end
end


script.on_init(InitRuinsFactions)
script.on_configuration_changed(InitRuinsFactions)


script.on_event(defines.events.on_player_selected_area, function(event)
  if event.item == "ruins-assimilator" then
    -- try to assimilate ruins
    assimilateRuins(event, False)
  end
end)


script.on_event(defines.events.on_player_alt_selected_area, function(event)
  if event.item == "ruins-assimilator" then
    -- try to assimilate ruins and mark for deconstruction where possible
    assimilateRuins(event, True)
  end
end)
