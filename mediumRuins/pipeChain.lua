
return function(center) --long chain of pipes
    local ce = game.surfaces[1].create_entity --save typing
    local fRN = game.forces['ruins_neutral']
    local direct = defines.direction

    ce{name = "pipe-to-ground", position = {center.x + (-1.0), center.y + (-7.0)}, force = fRN}
    ce{name = "pipe-to-ground", position = {center.x + (-1.0), center.y + (-2.0)}, direction = direct.south, force = fRN}
    ce{name = "pipe-to-ground", position = {center.x + (-7.0), center.y + (0.0)}, direction = direct.west, force = fRN}
    ce{name = "pipe-to-ground", position = {center.x + (-1.0), center.y + (-1.0)}, force = fRN}
    ce{name = "pipe-to-ground", position = {center.x + (4.0), center.y + (0.0)}, direction = direct.west, force = fRN}
    ce{name = "pipe-to-ground", position = {center.x + (3.0), center.y + (0.0)}, direction = direct.east, force = fRN}
    ce{name = "pipe-to-ground", position = {center.x + (-1.0), center.y + (6.0)}, direction = direct.south, force = fRN}
end
