return function(center)
    local ce = game.surfaces[1].create_entity --save typing
    local fN = game.forces.neutral
    local fRN = game.forces['ruins_neutral']

    ce{name = "tree-05", position = {center.x + (-2.5), center.y + (-5.5)}, force = fN}
    ce{name = "tree-05", position = {center.x + (0.5), center.y + (-6.5)}, force = fN}
    ce{name = "tree-05", position = {center.x + (3.5), center.y + (-5.5)}, force = fN}
    ce{name = "tree-05", position = {center.x + (-4.5), center.y + (-3.5)}, force = fN}
    ce{name = "tree-05", position = {center.x + (5.5), center.y + (-2.5)}, force = fN}
    ce{name = "tree-05", position = {center.x + (-6.5), center.y + (-0.5)}, force = fN}
    ce{name = "tree-05", position = {center.x + (6.5), center.y + (0.5)}, force = fN}
    ce{name = "tree-05", position = {center.x + (-5.5), center.y + (2.5)}, force = fN}
    ce{name = "tree-05", position = {center.x + (5.5), center.y + (3.5)}, force = fN}
    ce{name = "tree-05", position = {center.x + (-3.5), center.y + (5.5)}, force = fN}
    ce{name = "tree-05", position = {center.x + (0.5), center.y + (6.5)}, force = fN}
    ce{name = "tree-05", position = {center.x + (3.5), center.y + (5.5)}, force = fN}

    game.surfaces[1].set_tiles({
            {name = "water", position = {center.x + (-2.0), center.y + (-1.0)}},
            {name = "water", position = {center.x + (-2.0), center.y + (2.0)}},
            {name = "water", position = {center.x + (-1.0), center.y + (-2.0)}},
            {name = "water", position = {center.x + (-1.0), center.y + (-1.0)}},
            {name = "water", position = {center.x + (-1.0), center.y + (0.0)}},
            {name = "water", position = {center.x + (-1.0), center.y + (1.0)}},
            {name = "water", position = {center.x + (0.0), center.y + (-2.0)}},
            {name = "water", position = {center.x + (0.0), center.y + (-1.0)}},
            {name = "water", position = {center.x + (0.0), center.y + (0.0)}},
            {name = "water", position = {center.x + (0.0), center.y + (1.0)}},
            {name = "water", position = {center.x + (0.0), center.y + (2.0)}},
            {name = "water", position = {center.x + (1.0), center.y + (-3.0)}},
            {name = "water", position = {center.x + (1.0), center.y + (-2.0)}},
            {name = "water", position = {center.x + (1.0), center.y + (-1.0)}},
            {name = "water", position = {center.x + (1.0), center.y + (0.0)}},
            {name = "water", position = {center.x + (1.0), center.y + (1.0)}},
            {name = "water", position = {center.x + (1.0), center.y + (2.0)}},
            {name = "water", position = {center.x + (2.0), center.y + (-2.0)}},
            {name = "water", position = {center.x + (2.0), center.y + (-1.0)}},
            {name = "water", position = {center.x + (2.0), center.y + (0.0)}},
            {name = "water", position = {center.x + (2.0), center.y + (1.0)}},
            {name = "water", position = {center.x + (2.0), center.y + (2.0)}},
            {name = "water", position = {center.x + (3.0), center.y + (0.0)}},
                               }, true)


end
