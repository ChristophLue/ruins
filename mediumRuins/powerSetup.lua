return function(center) --power setup
    local ce = game.surfaces[1].create_entity --save typing
    local fRN = game.forces['ruins_neutral']
    local direct = defines.direction

    ce{name = "medium-electric-pole", position = {center.x + (3.0), center.y + (-6.0)}, force = fRN}
    ce{name = "steam-engine", position = {center.x + (-4.0), center.y + (0.0)}, force = fRN}
    ce{name = "medium-electric-pole", position = {center.x + (-1.0), center.y + (-3.0)}, force = fRN}
    ce{name = "steam-engine", position = {center.x + (-1.0), center.y + (0.0)}, force = fRN}
    ce{name = "steam-engine", position = {center.x + (2.0), center.y + (0.0)}, force = fRN}
    ce{name = "boiler", position = {center.x + (-4.0), center.y + (3.5)}, force = fRN}
    ce{name = "boiler", position = {center.x + (-1.0), center.y + (3.5)}, force = fRN}
    ce{name = "boiler", position = {center.x + (2.0), center.y + (3.5)}, force = fRN}
    ce{name = "transport-belt", position = {center.x + (-5.0), center.y + (6.0)}, direction = direct.east, force = fRN}
    ce{name = "burner-inserter", position = {center.x + (-4.0), center.y + (5.0)}, direction = direct.south, force = fRN}
    ce{name = "transport-belt", position = {center.x + (-3.0), center.y + (6.0)}, direction = direct.east, force = fRN}
    ce{name = "transport-belt", position = {center.x + (-4.0), center.y + (6.0)}, direction = direct.east, force = fRN}
    ce{name = "burner-inserter", position = {center.x + (-1.0), center.y + (5.0)}, direction = direct.south, force = fRN}
    ce{name = "transport-belt", position = {center.x + (-1.0), center.y + (6.0)}, direction = direct.east, force = fRN}
    ce{name = "transport-belt", position = {center.x + (-2.0), center.y + (6.0)}, direction = direct.east, force = fRN}
    ce{name = "transport-belt", position = {center.x + (1.0), center.y + (6.0)}, direction = direct.east, force = fRN}
    ce{name = "transport-belt", position = {center.x + (0.0), center.y + (6.0)}, direction = direct.east, force = fRN}
    ce{name = "burner-inserter", position = {center.x + (2.0), center.y + (5.0)}, direction = direct.south, force = fRN}
    ce{name = "transport-belt", position = {center.x + (2.0), center.y + (6.0)}, direction = direct.east, force = fRN}
end
