
return function(center) --victory poles
    local ce = game.surfaces[1].create_entity --save typing
    local fRN = game.forces['ruins_neutral']
    ce{name = "small-electric-pole", position = {center.x + (0.0), center.y + (-2.0)}, force = fRN}
    ce{name = "small-electric-pole", position = {center.x + (-2.0), center.y + (0.0)}, force = fRN}
    ce{name = "medium-electric-pole", position = {center.x + (0.0), center.y + (-1.0)}, force = fRN}
    ce{name = "small-electric-pole", position = {center.x + (2.0), center.y + (0.0)}, force = fRN}
    ce{name = "small-electric-pole", position = {center.x + (0.0), center.y + (1.0)}, force = fRN}
end
