return function(center) --land mine bunker
    local ce = game.surfaces[1].create_entity --save typing
    local fRN = game.forces['ruins_neutral']
    local direct = defines.direction
    ce{name = "stone-wall", position = {center.x-2, center.y}, force = fRN}
    ce{name = "stone-wall", position = {center.x-2, center.y-1}, force = fRN}
    ce{name = "stone-wall", position = {center.x-2, center.y-2}, force = fRN}
    ce{name = "stone-wall", position = {center.x + 1, center.y-1}, force = fRN}
    ce{name = "stone-wall", position = {center.x + 2, center.y-2}, force = fRN}
    ce{name = "stone-wall", position = {center.x + 2, center.y-1}, force = fRN}
    ce{name = "stone-wall", position = {center.x + 2, center.y}, force = fRN}
    ce{name = "stone-wall", position = {center.x + 2, center.y + 1}, force = fRN}
    ce{name = "stone-wall", position = {center.x + 2, center.y + 2}, force = fRN}
    ce{name = "stone-wall", position = {center.x + 2, center.y + 3}, force = fRN}
    ce{name = "stone-wall", position = {center.x + 1, center.y + 3}, force = fRN}
    ce{name = "stone-wall", position = {center.x, center.y + 3}, force = fRN}

    ce{name = "land-mine", position = {center.x, center.y}, force = game.forces['ruins_enemy']} --trap

end
