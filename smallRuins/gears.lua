return function(center) --small gears setup
    local ce = game.surfaces[1].create_entity --save typing
    local fRN = game.forces['ruins_neutral']
    local direct = defines.direction
    ce{name="transport-belt", position={center.x + (0.0), center.y + (-3.0)}, direction=defines.direction.east, force=fRN}
    ce{name="transport-belt", position={center.x + (0.0), center.y + (-2.0)}, force=fRN}
    ce{name="transport-belt", position={center.x + (-1.0), center.y + (-2.0)}, direction=defines.direction.east, force=fRN}
    ce{name="transport-belt", position={center.x + (2.0), center.y + (-3.0)}, direction=defines.direction.east, force=fRN}
    ce{name="transport-belt", position={center.x + (1.0), center.y + (-3.0)}, direction=defines.direction.east, force=fRN}
    ce{name="transport-belt", position={center.x + (1.0), center.y + (-2.0)}, direction=defines.direction.west, force=fRN}
    ce{name="transport-belt", position={center.x + (3.0), center.y + (-2.0)}, direction=defines.direction.east, force=fRN}
    ce{name="transport-belt", position={center.x + (3.0), center.y + (-3.0)}, direction=defines.direction.south, force=fRN}
    ce{name="assembling-machine-2", position={center.x + (-2.0), center.y + (1.0)}, force=fRN}
    ce{name="medium-electric-pole", position={center.x + (0.0), center.y + (-1.0)}, force=fRN}
    ce{name="fast-inserter", position={center.x + (-1.0), center.y + (-1.0)}, direction=defines.direction.south, force=fRN}
    ce{name="assembling-machine-2", position={center.x + (1.0), center.y + (1.0)}, force=fRN}
    ce{name="fast-inserter", position={center.x + (1.0), center.y + (-1.0)}, direction=defines.direction.south, force=fRN}
    ce{name="fast-inserter", position={center.x + (-2.0), center.y + (3.0)}, direction=defines.direction.south, force=fRN}
    ce{name="fast-inserter", position={center.x + (-3.0), center.y + (3.0)}, direction=defines.direction.south, force=fRN}
    ce{name="fast-inserter", position={center.x + (0.0), center.y + (3.0)}, direction=defines.direction.south, force=fRN}
    ce{name="medium-electric-pole", position={center.x + (-1.0), center.y + (3.0)}, force=fRN}
    ce{name="fast-inserter", position={center.x + (1.0), center.y + (3.0)}, direction=defines.direction.south, force=fRN}

end
